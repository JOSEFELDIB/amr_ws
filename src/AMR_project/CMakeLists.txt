cmake_minimum_required(VERSION 3.5)
project(AMR_project)

# Default to C++17
if(NOT CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 17)
endif()

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

find_package(ament_cmake REQUIRED)
find_package(rclcpp REQUIRED)
find_package(std_msgs REQUIRED)
find_package(geometry_msgs REQUIRED)
find_package(custom_topic REQUIRED)
find_package(visualization_msgs REQUIRED)


add_executable(order_optimizer src/OrderOptimizer_function.cpp)
ament_target_dependencies(order_optimizer rclcpp std_msgs geometry_msgs custom_topic visualization_msgs)

add_executable(talker src/publisher_member_function.cpp)
ament_target_dependencies(talker rclcpp std_msgs geometry_msgs custom_topic)

add_executable(listener src/subscriber_member_function.cpp)
ament_target_dependencies(listener rclcpp std_msgs visualization_msgs)

install(TARGETS
  order_optimizer
  talker
  listener
  DESTINATION lib/${PROJECT_NAME})
  
  

ament_package()
