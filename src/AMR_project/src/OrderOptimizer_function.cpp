

#include <chrono>
#include <memory>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <filesystem>
#include <thread>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "custom_topic/msg/next_order.hpp"
#include "visualization_msgs/msg/marker_array.hpp"





using std::placeholders::_1;
using std::placeholders::_2;

using namespace std::chrono_literals;



struct part {
    double cx,cy;         
    std::string part_name,product_name;
};  

int parsing_order(int order_id, double &cx, double &cy, std::vector <std::string> &products,std::string file,std::string folder_path ) {
    // the assumption is that the data in the files are all correct so no extra check will be done (for simplicity)
    // reading the files 
    //std::ifstream yaml_file("/home/josef/AMR_ws/src/AMR_project/applicants_amr_example_1/orders/"+file);
    std::ifstream yaml_file(folder_path+"orders/"+file);

    if (!yaml_file.is_open()) {
        std::cerr << "Error opening orders.YAML file." << std::endl;
        return 1;
    }

    std::string line;

    while (std::getline(yaml_file, line)) {
        
        // first condition part to check the word "order" in string form (to know which number will be the order)
        // second condition part to check if the given number exactly matches the condition and not part of the number in string      
        if (line.find("order:") != -1 && std::stoi(line.substr(8)) == order_id) {
            
                getline(yaml_file, line);
                cx = std::stod(line.substr(6));
                //  std::cout << "cx " << cx << "\n";
                getline(yaml_file, line);
                cy = std::stod(line.substr(6));
                //  std::cout << "cy " << cy << "\n";

                getline(yaml_file, line); // to remove the word line product
                // getting all the product name until reaching the next word "order" or reaching end of the file
                while (true) {
                    getline(yaml_file, line);

                    if (line.find("order") != -1)
                        break;

                    products.push_back(line.substr(4));
                }
                break;    
        

        }
        
    }

    yaml_file.close(); 
    return 0;

}

int parsing_whole_orders(int order_id, double& cx, double& cy, std::vector <std::string>& products,std::string folder_path) {

    // reading the required files
    //std::string orders_path = "/home/josef/AMR_ws/src/AMR_project/applicants_amr_example_1/orders";
    std::string orders_path = folder_path+"orders";
    std::vector<std::string> orders_files;

    for (auto& entry : std::filesystem::directory_iterator(orders_path)) {
        if (entry.is_regular_file() && entry.path().extension() == ".yaml") {
            orders_files.push_back(entry.path().filename().string());
        }
    }
 
    
  
   // multi threading
   std::vector<std::thread> threads;
   // assigning threads with the desired function and order files
   for (const auto& orders_file : orders_files) {
       threads.emplace_back(parsing_order, order_id, std::ref(cx), std::ref(cy), std::ref(products), std::ref(orders_file),std::ref(folder_path));
   }
   
   
   for (auto& thread : threads) {
        thread.join();
   }
    return 0;
}

int parsing_product(std::vector <std::string>& products, std::vector<part>& parts,std::string folder_path) {
 

    for (int i = 0; i < products.size(); i++) {
        // reading the files 
        //std::ifstream yaml_file("/home/josef/AMR_ws/src/AMR_project/applicants_amr_example_1/configuration/products.yaml");
        std::ifstream yaml_file(folder_path+"configuration/products.yaml");
        

        if (!yaml_file.is_open()) {
            std::cerr << "Error opening products.YAML file." << std::endl;
            return 1;
        }

        std::string line;

        while (true) {
           
            while (true)
                if (line.find("Product") != -1) 
                    break;   
                else 
                    std::getline(yaml_file, line);

                                      
            if (std::stoi(line.substr(19)) == std::stoi(products.at(i))) {


                std::getline(yaml_file, line);


                while (true) {
                    std::getline(yaml_file, line);
                    if (line.find("id") != -1)
                        break;
                    // creating the component variable
                    part component;
                    component.part_name = line.substr(line.find("Part") + 5, 1);

                    component.product_name = products.at(i);                

                    std::getline(yaml_file, line);
                    component.cx = std::stod(line.substr(7));                 

                    std::getline(yaml_file, line);
                    component.cy = std::stod(line.substr(7));                  

                    parts.push_back(component);
                }


                break;

            }
            else {
                // product not found and the new line needs to be fed to avoid infinite loop
                std::getline(yaml_file, line);

            }

        }
        yaml_file.close();
    }   

    return 0;

}

int calc_path(int order_id, double& cx, double& cy, double& cx_current, double& cy_current,std::vector <std::string>& products, std::vector<part>& parts,std::vector<part>& parts_marker, std::string description,std::string folder_path) {

    parsing_whole_orders(order_id, cx, cy, products,folder_path);
    
    // handeling invalid order entered by the user
    if (products.empty()) {
        std::cerr << "Invalid order"<<"\n";
        return -1;
    }
    
   parsing_product(products, parts,folder_path);
      
    
   // the there is not obstacle so the shortest path is a straight line 
   // no sqrt will be used to increase computation efficiency (distance squared is used)
   std::string path_message = "Working on order " + std::to_string(order_id) + " ( " + description + " )\n";
   int print_index = 1;
   std::vector<int> distance;


  // making a copy from the part vector
  parts_marker=parts;
   // calculating the shortest possible path
  
   while (parts.empty() != 1) {
       distance.clear();
       // calculating the distances between current position and each part position
       for (int i = 0; i < parts.size(); i++)
           distance.push_back(pow(parts.at(i).cx - cx_current, 2) + pow(parts.at(i).cy - cy_current, 2));


       double shortest_dist = distance.at(0);
       int shortest_dist_index = 0;      
       // find the index of the shortest position saved
       for (int i = 1; i < distance.size(); i++) {
           if (shortest_dist > distance.at(i)) {
               shortest_dist = distance.at(i);
               shortest_dist_index = i;

           }

       }

       // getting the shortest distance coordinates
       path_message += std::to_string(print_index) + ". Fetching part '" + parts.at(shortest_dist_index).part_name + "' for product '";
       path_message += parts.at(shortest_dist_index).product_name + "' at x:" + std::to_string(parts.at(shortest_dist_index).cx);
       path_message += ", y:" + std::to_string(parts.at(shortest_dist_index).cy) + "\n";


       // updating the robot position
       cx_current = parts.at(shortest_dist_index).cx;
       cy_current = parts.at(shortest_dist_index).cy;

       print_index++;

       // removing the processed part
       parts.erase(parts.begin() + shortest_dist_index);


   }
   path_message += std::to_string(print_index) + ". Delivering to destination x:" + std::to_string(cx) + ", y:" + std::to_string(cy)+ "\n";

   std::cout << path_message;

    return 0;
}

class Order_Opt : public rclcpp::Node
{
public:
  
  Order_Opt()
  : Node("OrderOptimizer"), count_(0)
  {  // input parameter for file path
      auto param_desc = rcl_interfaces::msg::ParameterDescriptor{};
      param_desc.description = "Enter the absolute path to the directory files such that the file 'applicants_amr_example_1' is part of the path";

     // this->declare_parameter("folder_path", "/home/josef/AMR_ws/src/AMR_project/applicants_amr_example_1/", param_desc);
      this->declare_parameter("folder_path", "./src/AMR_project/applicants_amr_example_1/", param_desc);
       // std::cout <<"std::filesystem::current_path()" << std::filesystem::current_path() << "---------------------------------------------------\n";



      // all subscriptions
      subscription1_ = this->create_subscription<geometry_msgs::msg::PoseStamped>(
      "currentPosition", 10, std::bind(&Order_Opt::currentPosition_callback, this, _1));

      subscription2_ = this->create_subscription<custom_topic::msg::NextOrder>(
      "nextOrder", 10, std::bind(&Order_Opt::nextOrder_callback, this, _1));
      
    
    

      // publisher for marker array
      publisher_ = this->create_publisher<visualization_msgs::msg::MarkerArray>("order_path", 10);

    

    // timer folder path 
    timer_folder_path_ = this->create_wall_timer(
      1000ms, std::bind(&Order_Opt::timer_folder_path_callback, this));

    // timer for calculating the geometic path
    timer__geometric_path_ = this->create_wall_timer(
      1000ms, std::bind(&Order_Opt::geometric_path_callback, this));

     // timer for marker array
    timer_marker_ = this->create_wall_timer(
      1000ms, std::bind(&Order_Opt::timer_marker_callback, this));


      
  }

  
  


private:
  // variables used in the functions of the class
  std::string folder_path;
  geometry_msgs::msg::PoseStamped::SharedPtr currentPosition_value;
  custom_topic::msg::NextOrder::SharedPtr nextOrder_value;
  std::vector<part> parts_marker; // needed to create copy from geometric path and use it in marker_array
 
  

  // subscriber functions
  void currentPosition_callback( geometry_msgs::msg::PoseStamped::SharedPtr msg) 
  {
  
    RCLCPP_INFO(this->get_logger(), "The PoseStamped message is: (%f, %f)",
                msg->pose.position.x, msg->pose.position.y);

    currentPosition_value=msg;
  }

   void nextOrder_callback( custom_topic::msg::NextOrder::SharedPtr msg)
  {
    RCLCPP_INFO(this->get_logger(), "The NextOrder message: Order ID = %u, Description = %s",
                msg->order_id, msg->description.c_str());
    nextOrder_value=msg;

  }

  // The function needed for printing the calculated path
  void geometric_path_callback(){
  
   // those values are from subscribed topics
    int order_id = nextOrder_value->order_id;
    double cx_current = currentPosition_value->pose.position.x;
    double cy_current = currentPosition_value->pose.position.y;
    std::string description = nextOrder_value->description.c_str();

    // the variables needed for the function
    double cx=0, cy=0; // getting the place of product
    std::vector <std::string> products;
    std::vector<part> parts;

    calc_path(order_id, cx, cy, cx_current, cy_current, products, parts,parts_marker,description,folder_path);
  

 
  }

  // timer for getting the path of the files
  void timer_folder_path_callback()
  { 
     // for getting input
      folder_path = this->get_parameter("folder_path").as_string();

      RCLCPP_INFO(this->get_logger(), "The folder path is %s!", folder_path.c_str());

      std::vector<rclcpp::Parameter> all_new_parameters{rclcpp::Parameter("folder_path", folder_path)};
      this->set_parameters(all_new_parameters);
      
  }

  void timer_marker_callback()
  {     

      visualization_msgs::msg::MarkerArray marker_array_;
     // marker_array_.markers.reserve(5);
      marker_array_.markers.clear();

    // adding cube that represents the position of the robot
    visualization_msgs::msg::Marker marker;
            marker.header.frame_id = "map";
            marker.type = visualization_msgs::msg::Marker::CUBE;
            marker.action = visualization_msgs::msg::Marker::ADD;

            marker.pose.position.x = currentPosition_value->pose.position.x; // the AMR_position
            marker.pose.position.y = currentPosition_value->pose.position.y;
            marker.pose.position.z = 0;

            marker.scale.x = 0.4;
            marker.scale.y = 0.4;
            marker.scale.z = 0.4;

            marker.color.a = 0.0;
            marker.color.r = 0.0;
            marker.color.g = 0.0;
            marker.color.b = 1.0;  // the robot has the color blue

    marker_array_.markers.push_back(marker); // Add the marker to the array

    // adding cylinder for each pickup location which are the parts only 
    for (int i = 0; i < parts_marker.size(); i++){
        marker.header.frame_id = "map";
            marker.type = visualization_msgs::msg::Marker::CYLINDER;
            marker.action = visualization_msgs::msg::Marker::ADD;

            marker.pose.position.x = parts_marker.at(i).cx; // the AMR_position
            marker.pose.position.y = parts_marker.at(i).cy;
            marker.pose.position.z = 0;

            marker.scale.x = 0.2;
            marker.scale.y = 0.2;
            marker.scale.z = 0.2;

            // the parts have the color red
            marker.color.a = 0.0;
            marker.color.r = 1.0;
            marker.color.g = 0.0;
            marker.color.b = 0.0; 

    marker_array_.markers.push_back(marker); // Add the marker to the array
    
    }
          
    publisher_->publish(marker_array_); // Publish the marker array
  }
  


  // subscription pointers
  rclcpp::Subscription<geometry_msgs::msg::PoseStamped>::SharedPtr subscription1_;
  rclcpp::Subscription<custom_topic::msg::NextOrder>::SharedPtr subscription2_;


  //timer pointers
  rclcpp::TimerBase::SharedPtr timer_folder_path_;
  rclcpp::TimerBase::SharedPtr timer__geometric_path_;
  rclcpp::TimerBase::SharedPtr timer_marker_;

  // publisher pointers
  rclcpp::Publisher<visualization_msgs::msg::MarkerArray>::SharedPtr publisher_;

  size_t count_;


};



int main(int argc, char * argv[])
{   
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<Order_Opt>());
  rclcpp::shutdown();
  return 0;
}

