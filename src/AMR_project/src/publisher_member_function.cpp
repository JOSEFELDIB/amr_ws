// Copyright 2016 Open Source Robotics Foundation, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "geometry_msgs/msg/pose_stamped.hpp"
#include "custom_topic/msg/next_order.hpp"

using namespace std::chrono_literals;

/* This example creates a subclass of Node and uses std::bind() to register a
 * member function as a callback from the timer. */

class MinimalPublisher : public rclcpp::Node
{
public:
  MinimalPublisher()
  : Node("minimal_publisher"), count_(0)
  {
  
    publisher1_ = this->create_publisher<geometry_msgs::msg::PoseStamped>("currentPosition", 10);
    publisher2_ = this->create_publisher<custom_topic::msg::NextOrder>("nextOrder", 10);

    // changing order_id to test the result
    this->declare_parameter("order_id_index", 1000001);

    timer_ = this->create_wall_timer(
      1000ms, std::bind(&MinimalPublisher::timer_callback, this));

    
    timer1_ = this->create_wall_timer(
      500ms, std::bind(&MinimalPublisher::timer_currentPosition_callback, this));
   
    timer2_ = this->create_wall_timer(
      500ms, std::bind(&MinimalPublisher::timer_nextOrder_callback, this));

  }

private:
  int order_id_index;
  void timer_callback()
    {
      // Get the order ID index value from the parameter
      order_id_index = this->get_parameter("order_id_index").as_int();

      RCLCPP_INFO(this->get_logger(), "the order id index is %d!", order_id_index);

      std::vector<rclcpp::Parameter> all_new_parameters{rclcpp::Parameter("order_id_index", order_id_index)};
      this->set_parameters(all_new_parameters);

    }



  void timer_currentPosition_callback()
  {
    auto message = geometry_msgs::msg::PoseStamped();

    // temporary values for testing !!
    message.pose.position.x = 10;
    message.pose.position.y = 5; 

    RCLCPP_INFO(this->get_logger(), "Publishing: x = %f, y = %f", message.pose.position.x, message.pose.position.y);
    publisher1_->publish(message);
  }

   void timer_nextOrder_callback()
  {
    auto message = custom_topic::msg::NextOrder();

    // temporary values for testing !!
    message.order_id = order_id_index; 
    message.description = "getting the needed AMR order";

    RCLCPP_INFO(this->get_logger(), "Publishing NextOrder message: order_id = %u, description = %s", message.order_id, message.description.c_str());
    publisher2_->publish(message);

  }

  rclcpp::TimerBase::SharedPtr timer_;
  rclcpp::TimerBase::SharedPtr timer1_;
  rclcpp::TimerBase::SharedPtr timer2_;

  rclcpp::Publisher<geometry_msgs::msg::PoseStamped>::SharedPtr publisher1_;
  rclcpp::Publisher<custom_topic::msg::NextOrder>::SharedPtr publisher2_;
  size_t count_;
};

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<MinimalPublisher>());
  rclcpp::shutdown();
  return 0;
}
