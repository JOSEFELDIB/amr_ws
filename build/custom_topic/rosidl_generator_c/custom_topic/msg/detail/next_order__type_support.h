// generated from rosidl_generator_c/resource/idl__type_support.h.em
// with input from custom_topic:msg/NextOrder.idl
// generated code does not contain a copyright notice

#ifndef CUSTOM_TOPIC__MSG__DETAIL__NEXT_ORDER__TYPE_SUPPORT_H_
#define CUSTOM_TOPIC__MSG__DETAIL__NEXT_ORDER__TYPE_SUPPORT_H_

#include "rosidl_typesupport_interface/macros.h"

#include "custom_topic/msg/rosidl_generator_c__visibility_control.h"

#ifdef __cplusplus
extern "C"
{
#endif

#include "rosidl_runtime_c/message_type_support_struct.h"

// Forward declare the get type support functions for this type.
ROSIDL_GENERATOR_C_PUBLIC_custom_topic
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(
  rosidl_typesupport_c,
  custom_topic,
  msg,
  NextOrder
)();

#ifdef __cplusplus
}
#endif

#endif  // CUSTOM_TOPIC__MSG__DETAIL__NEXT_ORDER__TYPE_SUPPORT_H_
