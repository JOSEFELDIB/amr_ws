// generated from rosidl_generator_cpp/resource/idl__traits.hpp.em
// with input from custom_topic:msg/NextOrder.idl
// generated code does not contain a copyright notice

#ifndef CUSTOM_TOPIC__MSG__DETAIL__NEXT_ORDER__TRAITS_HPP_
#define CUSTOM_TOPIC__MSG__DETAIL__NEXT_ORDER__TRAITS_HPP_

#include "custom_topic/msg/detail/next_order__struct.hpp"
#include <rosidl_runtime_cpp/traits.hpp>
#include <stdint.h>
#include <type_traits>

namespace rosidl_generator_traits
{

template<>
inline const char * data_type<custom_topic::msg::NextOrder>()
{
  return "custom_topic::msg::NextOrder";
}

template<>
inline const char * name<custom_topic::msg::NextOrder>()
{
  return "custom_topic/msg/NextOrder";
}

template<>
struct has_fixed_size<custom_topic::msg::NextOrder>
  : std::integral_constant<bool, false> {};

template<>
struct has_bounded_size<custom_topic::msg::NextOrder>
  : std::integral_constant<bool, false> {};

template<>
struct is_message<custom_topic::msg::NextOrder>
  : std::true_type {};

}  // namespace rosidl_generator_traits

#endif  // CUSTOM_TOPIC__MSG__DETAIL__NEXT_ORDER__TRAITS_HPP_
