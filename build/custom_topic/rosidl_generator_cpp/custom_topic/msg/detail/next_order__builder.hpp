// generated from rosidl_generator_cpp/resource/idl__builder.hpp.em
// with input from custom_topic:msg/NextOrder.idl
// generated code does not contain a copyright notice

#ifndef CUSTOM_TOPIC__MSG__DETAIL__NEXT_ORDER__BUILDER_HPP_
#define CUSTOM_TOPIC__MSG__DETAIL__NEXT_ORDER__BUILDER_HPP_

#include "custom_topic/msg/detail/next_order__struct.hpp"
#include <rosidl_runtime_cpp/message_initialization.hpp>
#include <algorithm>
#include <utility>


namespace custom_topic
{

namespace msg
{

namespace builder
{

class Init_NextOrder_description
{
public:
  explicit Init_NextOrder_description(::custom_topic::msg::NextOrder & msg)
  : msg_(msg)
  {}
  ::custom_topic::msg::NextOrder description(::custom_topic::msg::NextOrder::_description_type arg)
  {
    msg_.description = std::move(arg);
    return std::move(msg_);
  }

private:
  ::custom_topic::msg::NextOrder msg_;
};

class Init_NextOrder_order_id
{
public:
  Init_NextOrder_order_id()
  : msg_(::rosidl_runtime_cpp::MessageInitialization::SKIP)
  {}
  Init_NextOrder_description order_id(::custom_topic::msg::NextOrder::_order_id_type arg)
  {
    msg_.order_id = std::move(arg);
    return Init_NextOrder_description(msg_);
  }

private:
  ::custom_topic::msg::NextOrder msg_;
};

}  // namespace builder

}  // namespace msg

template<typename MessageType>
auto build();

template<>
inline
auto build<::custom_topic::msg::NextOrder>()
{
  return custom_topic::msg::builder::Init_NextOrder_order_id();
}

}  // namespace custom_topic

#endif  // CUSTOM_TOPIC__MSG__DETAIL__NEXT_ORDER__BUILDER_HPP_
