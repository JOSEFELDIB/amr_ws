# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/josef/AMR_ws/build/custom_topic/rosidl_generator_py/custom_topic/msg/_next_order_s.c" "/home/josef/AMR_ws/build/custom_topic/CMakeFiles/custom_topic__python.dir/rosidl_generator_py/custom_topic/msg/_next_order_s.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "RCUTILS_ENABLE_FAULT_INJECTION"
  "ROS_PACKAGE_NAME=\"custom_topic\""
  "custom_topic__python_EXPORTS"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "rosidl_generator_c"
  "rosidl_generator_py"
  "/usr/include/python3.8"
  "rosidl_typesupport_c"
  "/opt/ros/foxy/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/josef/AMR_ws/build/custom_topic/CMakeFiles/custom_topic__rosidl_generator_c.dir/DependInfo.cmake"
  "/home/josef/AMR_ws/build/custom_topic/CMakeFiles/custom_topic__rosidl_typesupport_c.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
