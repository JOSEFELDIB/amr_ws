// generated from rosidl_typesupport_introspection_c/resource/idl__type_support.c.em
// with input from custom_topic:msg/NextOrder.idl
// generated code does not contain a copyright notice

#include <stddef.h>
#include "custom_topic/msg/detail/next_order__rosidl_typesupport_introspection_c.h"
#include "custom_topic/msg/rosidl_typesupport_introspection_c__visibility_control.h"
#include "rosidl_typesupport_introspection_c/field_types.h"
#include "rosidl_typesupport_introspection_c/identifier.h"
#include "rosidl_typesupport_introspection_c/message_introspection.h"
#include "custom_topic/msg/detail/next_order__functions.h"
#include "custom_topic/msg/detail/next_order__struct.h"


// Include directives for member types
// Member `description`
#include "rosidl_runtime_c/string_functions.h"

#ifdef __cplusplus
extern "C"
{
#endif

void NextOrder__rosidl_typesupport_introspection_c__NextOrder_init_function(
  void * message_memory, enum rosidl_runtime_c__message_initialization _init)
{
  // TODO(karsten1987): initializers are not yet implemented for typesupport c
  // see https://github.com/ros2/ros2/issues/397
  (void) _init;
  custom_topic__msg__NextOrder__init(message_memory);
}

void NextOrder__rosidl_typesupport_introspection_c__NextOrder_fini_function(void * message_memory)
{
  custom_topic__msg__NextOrder__fini(message_memory);
}

static rosidl_typesupport_introspection_c__MessageMember NextOrder__rosidl_typesupport_introspection_c__NextOrder_message_member_array[2] = {
  {
    "order_id",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_UINT32,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    false,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(custom_topic__msg__NextOrder, order_id),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  },
  {
    "description",  // name
    rosidl_typesupport_introspection_c__ROS_TYPE_STRING,  // type
    0,  // upper bound of string
    NULL,  // members of sub message
    false,  // is array
    0,  // array size
    false,  // is upper bound
    offsetof(custom_topic__msg__NextOrder, description),  // bytes offset in struct
    NULL,  // default value
    NULL,  // size() function pointer
    NULL,  // get_const(index) function pointer
    NULL,  // get(index) function pointer
    NULL  // resize(index) function pointer
  }
};

static const rosidl_typesupport_introspection_c__MessageMembers NextOrder__rosidl_typesupport_introspection_c__NextOrder_message_members = {
  "custom_topic__msg",  // message namespace
  "NextOrder",  // message name
  2,  // number of fields
  sizeof(custom_topic__msg__NextOrder),
  NextOrder__rosidl_typesupport_introspection_c__NextOrder_message_member_array,  // message members
  NextOrder__rosidl_typesupport_introspection_c__NextOrder_init_function,  // function to initialize message memory (memory has to be allocated)
  NextOrder__rosidl_typesupport_introspection_c__NextOrder_fini_function  // function to terminate message instance (will not free memory)
};

// this is not const since it must be initialized on first access
// since C does not allow non-integral compile-time constants
static rosidl_message_type_support_t NextOrder__rosidl_typesupport_introspection_c__NextOrder_message_type_support_handle = {
  0,
  &NextOrder__rosidl_typesupport_introspection_c__NextOrder_message_members,
  get_message_typesupport_handle_function,
};

ROSIDL_TYPESUPPORT_INTROSPECTION_C_EXPORT_custom_topic
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, custom_topic, msg, NextOrder)() {
  if (!NextOrder__rosidl_typesupport_introspection_c__NextOrder_message_type_support_handle.typesupport_identifier) {
    NextOrder__rosidl_typesupport_introspection_c__NextOrder_message_type_support_handle.typesupport_identifier =
      rosidl_typesupport_introspection_c__identifier;
  }
  return &NextOrder__rosidl_typesupport_introspection_c__NextOrder_message_type_support_handle;
}
#ifdef __cplusplus
}
#endif
