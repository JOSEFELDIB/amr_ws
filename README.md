- The project constists of 3 nodes 
    - First node in the publisher node --> minimal_publisher
                - this node takes from the user the parameter order id but if not given there is a default order id which is 1000001
                - this node publishes the order id and the description of the node

    - Second node in the Order_Optimizer node -->OrderOptimizer
                - this node subscribes to twos topic published from the first node (which are ‘currentPosition’ and ‘nextOrder’)
                - this node publish a marker array to the third node (whci is ‘order_path' )
                - this node takes a parameter for file path as an input from user but if not given there is a default path (which functions correctly)
                - this node calculates the shortest path traveled from current position till reaching the final destination 
                
    - third node is the subscriber node --> minimal_subscriber
                - this node subscribes to the topic published from second node and displays the message of the marker array


- There is a .yml file used for testing using some orders to see the different results