// generated from rosidl_generator_cpp/resource/idl.hpp.em
// generated code does not contain a copyright notice

#ifndef CUSTOM_TOPIC__MSG__NEXT_ORDER_HPP_
#define CUSTOM_TOPIC__MSG__NEXT_ORDER_HPP_

#include "custom_topic/msg/detail/next_order__struct.hpp"
#include "custom_topic/msg/detail/next_order__builder.hpp"
#include "custom_topic/msg/detail/next_order__traits.hpp"
#include "custom_topic/msg/detail/next_order__type_support.hpp"

#endif  // CUSTOM_TOPIC__MSG__NEXT_ORDER_HPP_
