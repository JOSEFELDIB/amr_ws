// generated from rosidl_generator_c/resource/idl__struct.h.em
// with input from custom_topic:msg/NextOrder.idl
// generated code does not contain a copyright notice

#ifndef CUSTOM_TOPIC__MSG__DETAIL__NEXT_ORDER__STRUCT_H_
#define CUSTOM_TOPIC__MSG__DETAIL__NEXT_ORDER__STRUCT_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


// Constants defined in the message

// Include directives for member types
// Member 'description'
#include "rosidl_runtime_c/string.h"

// Struct defined in msg/NextOrder in the package custom_topic.
typedef struct custom_topic__msg__NextOrder
{
  uint32_t order_id;
  rosidl_runtime_c__String description;
} custom_topic__msg__NextOrder;

// Struct for a sequence of custom_topic__msg__NextOrder.
typedef struct custom_topic__msg__NextOrder__Sequence
{
  custom_topic__msg__NextOrder * data;
  /// The number of valid items in data
  size_t size;
  /// The number of allocated items in data
  size_t capacity;
} custom_topic__msg__NextOrder__Sequence;

#ifdef __cplusplus
}
#endif

#endif  // CUSTOM_TOPIC__MSG__DETAIL__NEXT_ORDER__STRUCT_H_
