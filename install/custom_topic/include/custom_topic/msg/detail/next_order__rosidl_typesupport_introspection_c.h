// generated from rosidl_typesupport_introspection_c/resource/idl__rosidl_typesupport_introspection_c.h.em
// with input from custom_topic:msg/NextOrder.idl
// generated code does not contain a copyright notice

#ifndef CUSTOM_TOPIC__MSG__DETAIL__NEXT_ORDER__ROSIDL_TYPESUPPORT_INTROSPECTION_C_H_
#define CUSTOM_TOPIC__MSG__DETAIL__NEXT_ORDER__ROSIDL_TYPESUPPORT_INTROSPECTION_C_H_

#ifdef __cplusplus
extern "C"
{
#endif


#include "rosidl_runtime_c/message_type_support_struct.h"
#include "rosidl_typesupport_interface/macros.h"
#include "custom_topic/msg/rosidl_typesupport_introspection_c__visibility_control.h"

ROSIDL_TYPESUPPORT_INTROSPECTION_C_PUBLIC_custom_topic
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_introspection_c, custom_topic, msg, NextOrder)();

#ifdef __cplusplus
}
#endif

#endif  // CUSTOM_TOPIC__MSG__DETAIL__NEXT_ORDER__ROSIDL_TYPESUPPORT_INTROSPECTION_C_H_
