// generated from rosidl_generator_c/resource/idl.h.em
// with input from custom_topic:msg/NextOrder.idl
// generated code does not contain a copyright notice

#ifndef CUSTOM_TOPIC__MSG__NEXT_ORDER_H_
#define CUSTOM_TOPIC__MSG__NEXT_ORDER_H_

#include "custom_topic/msg/detail/next_order__struct.h"
#include "custom_topic/msg/detail/next_order__functions.h"
#include "custom_topic/msg/detail/next_order__type_support.h"

#endif  // CUSTOM_TOPIC__MSG__NEXT_ORDER_H_
